package principal;

import java.util.Scanner;

/**
 * 
 * @author Ivan Soro
 * @since Febrero 2018
 */

public class Generador {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		Metodos.mostrarMenu();
		int longitudPassword = Metodos.introducirLongitudCadena(scanner);
		int opcionElegida = Metodos.elegirOpcion(scanner);
		scanner.close();

		String password = Metodos.generarPassword(longitudPassword, opcionElegida);
		System.out.println(password);
	}

}
