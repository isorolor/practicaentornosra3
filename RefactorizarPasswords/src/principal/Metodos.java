package principal;

import java.util.Scanner;

public class Metodos {

	public static String letrasNumerosCaracteresEspeciales(String pass, int longitud) {
		int numeroAleatorio;
		char letra4;
		char caracter4;
		int numero4;

		for (int i = 0; i < longitud; i++) {
			numeroAleatorio = (int) (Math.random() * 3);

			if (numeroAleatorio == 1) {
				letra4 = generarLetraAleatoria();
				pass += letra4;
			} else if (numeroAleatorio == 2) {
				caracter4 = generarCaracterAleatorio();
				pass += caracter4;
			} else {
				numero4 = generarCifraAleatoria();
				pass += numero4;
			}
		}
		return pass;
	}

	public static String letrasYCaracteresEspeciales(String pass, int longitud) {
		char letra3;
		char caracter3;
		int numeroAleatorio;
		for (int i = 0; i < longitud; i++) {
			numeroAleatorio = (int) (Math.random() * 2);

			if (numeroAleatorio == 1) {
				letra3 = generarLetraAleatoria();
				pass += letra3;
			} else {
				caracter3 = generarCaracterAleatorio();
				pass += caracter3;
			}
		}
		return pass;
	}

	public static char generarCaracterAleatorio() {
		return ((char) ((Math.random() * 15) + 33));
	}

	public static String numeros09(String pass, int longitud) {
		int numero2;
		for (int i = 0; i < longitud; i++) {
			numero2 = generarCifraAleatoria();
			pass += numero2;
		}
		return pass;
	}

	public static int generarCifraAleatoria() {
		return ((int) (Math.random() * 10));
	}

	public static String caracteresAZ(String pass, int longitud) {
		char letra1;
		for (int i = 0; i < longitud; i++) {
			letra1 = generarLetraAleatoria();
			pass += letra1;
		}
		return pass;
	}

	/**
	 * Metodo que genera una letra aleatoria a partir de Math random
	 * 
	 * @return char caracter
	 */
	public static char generarLetraAleatoria() {
		return ((char) ((Math.random() * 26) + 65));
	}

	/**
	 * Metodo que contiene un Switch para que cada caso genere un tipo de
	 * contrasena distinto
	 * 
	 * @return String password
	 */

	public static String generarPassword(int longitudPassword, int opcionElegida) {

		String password = "";
		switch (opcionElegida) {
		case 1:
			password = caracteresAZ(password, longitudPassword);
			break;
		case 2:
			password = numeros09(password, longitudPassword);
			break;
		case 3:
			password = letrasYCaracteresEspeciales(password, longitudPassword);
			break;
		case 4:
			password = letrasNumerosCaracteresEspeciales(password, longitudPassword);
			break;
		}
		return password;
	}

	/**
	 * Metodo que pide el tipo de password y lo guarda en un int
	 * 
	 * @return int opcion
	 */
	public static int elegirOpcion(Scanner scanner) {
		System.out.println("Elige tipo de password: ");
		int opcion = scanner.nextInt();
		return opcion;
	}

	/**
	 * Metodo que pide la longitud del password
	 * 
	 * @return int longitud
	 */
	public static int introducirLongitudCadena(Scanner scanner) {
		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = scanner.nextInt();
		return longitud;
	}

	/**
	 * Metodo que muestra un menu con cuatro opciones
	 */
	public static void mostrarMenu() {
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
	}

}
