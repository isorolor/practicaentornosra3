package ra3.refactorizacion;

/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

//import java.*; //Sentencia import no utilizada. No deber�a estar
import java.util.Scanner; //Sentencia import: cada clase en una l�nea separada
// Lo correcto ser�a: import java.util.Scanner;

/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactorizar1 { // Nombre de las clases o interfaces:
								// UpperCamelCase.

	// Nombres de constantes: CONSTANT_CASE. Todo el may�sculas, separando con
	// barra baja.
	final static String SALUDO = "Bienvenido al programa";

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);

		System.out.println(SALUDO);
		System.out.println("Introduce tu dni");
		String dni = lector.nextLine();
		System.out.println("Introduce tu nombre");
		String nombre = lector.nextLine();

		// Scanner nunca se cierra, y se renombra a lector/scanner/sc
		lector.close();

		System.out.println(dni);
		System.out.println(nombre);
		// Cadena1 y cadena2 nunca se usan. Por eso aparecen los warnings

		// Las variables locales se deben inicializar en el momento de
		// declararlas o justo despu�s

		// Adem�s, solo se usan identificadores de un solo car�cter para
		// representar los contadores del bucle for
		int numeroA = 7;
		int numeroB = 16;
		int numeroC = 25;
		// Declaraciones de variables. Una sola declaraci�n por l�nea

		// En las sentencias de control de flujo (if, else, for, do-while,
		// try-catch-finally) = Una sola instrucci�n por linea.

		if (numeroA > numeroB || (numeroC % 5 != 0) && ((numeroC * 3) - 1) > (numeroB / numeroC)) {
			System.out.println("Se cumple la condici�n");
		}

		numeroC = (numeroA + (numeroB * numeroC) + (numeroB / numeroA));

		// Los arrays tienen los corchetes [ ] unidos a su tipo de datos:
		// String[] diasSemana = new String[7];
		// Los arrays se pueden inicializar en bloque
		String[] arrayDiasSemana = { "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", 
				"Sabado", "Domingo" };

		recorrerDiasSemana(arrayDiasSemana);

	}

	// El nombre correcto del m�todo ser�a recorrerArray, o directamente
	// llamarse de otra manera: recorrerDiasSemana
	static void recorrerDiasSemana(String vectorDeStrings[]) {
		// Variables locales, atributos de la clase, nombres de par�metros:
		// lowerCamelCase
		// Lo correcto ser�a llamar al array de String: vectorDeStrings[]
		for (int i = 0; i < vectorDeStrings.length; i++) {
			System.out.println(
				"El dia de la semana en el que te encuentras [" 
				+ (i + 1) + "-7] es el dia: " + vectorDeStrings[i]);
		}
	}

	// Concatenar String con el operador '+' tambi�n genera mucha carga, ya que
	// crea un nuevo String en memoria (Los objetos String son inmutables). Se
	// debe tratar de evitar siempre las concatenaciones (+) dentro de un bucle.
}