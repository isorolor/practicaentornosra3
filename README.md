﻿# Entornos de Desarrollo - Practica RA3 Refactorización
## Alumno: Iván Soro Lorente, 1º DAW
### Repositorio creado el 21/02/2018

 + Creados archivo readme.md y archivo .gitignore

22/02/2018

 + Añadida wiki

01/03/2018

 + Subido primer ejercicio de refactorización. Queda pendiente documentarlo en la wiki
 + Añado 2º ejercicio de refactorización. Queda pendiente documentarlo en la wiki

08/03/2018

 + Añadida a la wiki documentación de los dos primeros ejercicios
 + Subido tercer ejercicio de refactorización (RefactorizarPasswords). Queda pendiente documentarlo en la wiki

11/03/2018

 + Añadida a la wiki documentación del tercer ejercicio
 + Subido cuarto ejercicio de refactorización (RefactorizarAhorcado)
 + 4º ejercicio documentado en la wiki

12/03/2018

 + Ejercicio ahorcado actualizado (método mismoCaracter, caracteresElegidos)