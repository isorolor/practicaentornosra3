package principal;

import java.util.Scanner;

/**
 * 
 * @author Ivan Soro
 * @since Febrero 2018
 */

public class Principal {

	public static void main(String[] args) {

		String palabraSecreta;
		palabraSecreta = MetodosAhorcado.obtenerPalabra();
		Scanner lector = new Scanner(System.in);

		char[][] caracteresPalabra = MetodosAhorcado.crearMatriz(palabraSecreta);
		String caracteresElegidos = "";
		int fallos;
		boolean acertado;

		do {
			MetodosAhorcado.mostrarPalabra(caracteresPalabra);

			caracteresElegidos = MetodosAhorcado.caracteresElegidos(lector, caracteresElegidos);

			fallos = MetodosAhorcado.comprobarCaracter(caracteresPalabra, caracteresElegidos);

			MetodosAhorcado.dibujarAhorcado(fallos);

			acertado = MetodosAhorcado.comprobarFallos(palabraSecreta, caracteresPalabra, fallos);

		} while (MetodosAhorcado.juegoTerminado(fallos, acertado));

		lector.close();
	}

}