package principal;

import java.io.File;
import java.util.Scanner;

public class MetodosAhorcado {

	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];

	public static char[][] crearMatriz(String palabraSecreta) {
		
		char[][] caracteresPalabra = new char[2][];
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];
		return caracteresPalabra;
	}

	public static boolean juegoTerminado(int fallos, boolean acertado) {
		
		if (!acertado && fallos < FALLOS) {
			return true;
		}
		return false;
	}

	public static int comprobarCaracter(char[][] caracteresPalabra, String caracteresElegidos) {
		
		int fallos;
		fallos = 0;
		boolean encontrado;

		for (int j = 0; j < caracteresElegidos.length(); j++) {
			encontrado = false;
			encontrado = mismoCaracter(caracteresPalabra, caracteresElegidos, encontrado, j);
			if (!encontrado)
				fallos++;
		}
		return fallos;
	}

	public static boolean mismoCaracter(char[][] caracteresPalabra, String caracteresElegidos, boolean encontrado,
			int j) {
		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
				caracteresPalabra[1][i] = '1';
				encontrado = true;
			}
		}
		return encontrado;
	}

	public static String caracteresElegidos(Scanner lector, String caracteresElegidos) {
		
		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
		caracteresElegidos += lector.nextLine().toUpperCase();
		return caracteresElegidos;
	}

	public static void mostrarPalabra(char[][] caracteresPalabra) {
		
		System.out.println("Acierta la palabra");
		System.out.println("####################################");

		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		System.out.println();
	}

	public static void dibujarAhorcado(int fallos) {
		
		switch (fallos) {
		case 1:

			System.out.println("     ___");
			break;
		case 2:

			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
			break;
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
			break;
		}
	}

	public static boolean comprobarFallos(String palabraSecreta, char[][] caracteresPalabra, int fallos) {
		
		boolean acertado;
		if (fallos >= FALLOS) {
			System.out.println("Has perdido: " + palabraSecreta);
		}
		acertado = true;
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		if (acertado)
			System.out.println("Has Acertado: " + palabraSecreta);
		return acertado;
	}

	public static String obtenerPalabra() {

		String rutaArchivoTexto = "src\\palabras.txt";
		obtenerArrayPalabras(rutaArchivoTexto);
		String palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];
		return palabraSecreta;
	}

	public static void obtenerArrayPalabras(String rutaArchivoTexto) {

		File fichero = new File(rutaArchivoTexto);
		Scanner lectorFichero = null;

		try {
			lectorFichero = new Scanner(fichero);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = lectorFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fichero != null && lectorFichero != null)
				lectorFichero.close();
		}
	}

}
