package ra3.refactorizacion;

/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

// import java.io.*; //Sentencia import no utilizada.
import java.util.Scanner;

// Definici�n de una �nica clase o interface cuyo nombre es identico al nombre del fichero sin la extensi�n.

/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactorizar2 {

	static int cantidadMaximaAlumnos = 10;
	static Scanner lector;

	// Inicio de llave en la misma instrucci�n.
	public static void main(String[] args) {

		lector = new Scanner(System.in);

		// Declaraci�n de variables: Una sola declaraci�n por l�nea.
		// Adem�s, solo se usan identificadores de un solo car�cter para
		// representar los contadores del bucle for

		int[] arrayAlumnos = new int[cantidadMaximaAlumnos];

		// Los espacios en blanco mejoran la legibilidad
		for (int i = 0; i < arrayAlumnos.length; i++) {
			// Inicio de llave en la misma instrucci�n.

			System.out.println("Introduce nota media de alumno");
			arrayAlumnos[i] = lector.nextInt();
			// arrays[n] = a.nextLine(); Error en la construcci�n del Scanner
			// (es de int)
		}

		System.out.println("La media es: " + calcularMedia(arrayAlumnos));

		lector.close();
	}

	static double calcularMedia(int[] arrayConNotas) {

		// double c = 0; Identificador de un solo caracter
		double sumaTotal = 0;

		for (int i = 0; i < arrayConNotas.length; i++) {

			sumaTotal = sumaTotal + arrayConNotas[i];
		}
		return sumaTotal / 10;
	}

}
